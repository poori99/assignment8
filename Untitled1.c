#include <stdio.h>

struct student
{
    char student_name[50];
    char subject[20];
    float marks;
};

int main()
{
    struct student s1[20];
    int i;

    printf("Enter students information\n\n");
    for(i=0;i<5;i++)
    {
        printf("Enter student name: ");
        scanf("%s",s1[i].student_name);

        printf("Enter subject: ");
        scanf("%s",s1[i].subject);

        printf("Enter marks: ");
        scanf("%f",&s1[i].marks);

        printf("\n");
    }

    for(i=0;i<5;i++)
    {
        printf("\n Student %s has %.0f marks for subject %s.\n",s1[i].student_name,s1[i].marks,s1[i].subject);
    }

    return 0;
}
